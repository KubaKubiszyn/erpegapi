﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erpeg.Migrations
{
    public partial class InitalMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Professions",
                columns: table => new
                {
                    ProfessionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nazwa = table.Column<string>(nullable: true),
                    Opis = table.Column<string>(nullable: true),
                    Startowa = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professions", x => x.ProfessionId);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Imie = table.Column<string>(nullable: true),
                    KolorOczu = table.Column<string>(nullable: true),
                    KolorWlosow = table.Column<string>(nullable: true),
                    ZnakGwiezdny = table.Column<string>(nullable: true),
                    Plec = table.Column<string>(nullable: true),
                    Waga = table.Column<int>(nullable: true),
                    Wzrost = table.Column<int>(nullable: true),
                    Rodzenstwo = table.Column<int>(nullable: true),
                    ZnakiSzczegolne = table.Column<string>(nullable: true),
                    MiejsceUrodzenia = table.Column<string>(nullable: true),
                    DoswiadczenieObecne = table.Column<int>(nullable: true, defaultValue: 0),
                    DoswiadczenieRazem = table.Column<int>(nullable: true, defaultValue: 0),
                    ProfesjaProfessionId = table.Column<int>(nullable: true),
                    PoprzedniaProfesjaProfessionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.CharacterId);
                    table.ForeignKey(
                        name: "FK_Characters_Professions_PoprzedniaProfesjaProfessionId",
                        column: x => x.PoprzedniaProfesjaProfessionId,
                        principalTable: "Professions",
                        principalColumn: "ProfessionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Characters_Professions_ProfesjaProfessionId",
                        column: x => x.ProfesjaProfessionId,
                        principalTable: "Professions",
                        principalColumn: "ProfessionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PromotionProfessions",
                columns: table => new
                {
                    CurrentProfessionId = table.Column<int>(nullable: false),
                    NextProfessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromotionProfessions", x => new { x.CurrentProfessionId, x.NextProfessionId });
                    table.ForeignKey(
                        name: "FK_PromotionProfessions_Professions_CurrentProfessionId",
                        column: x => x.CurrentProfessionId,
                        principalTable: "Professions",
                        principalColumn: "ProfessionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PromotionProfessions_Professions_NextProfessionId",
                        column: x => x.NextProfessionId,
                        principalTable: "Professions",
                        principalColumn: "ProfessionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Stats",
                columns: table => new
                {
                    StatId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CharacterId = table.Column<int>(nullable: false),
                    Ww = table.Column<int>(nullable: true),
                    Us = table.Column<int>(nullable: true),
                    K = table.Column<int>(nullable: true),
                    Odp = table.Column<int>(nullable: true),
                    Zr = table.Column<int>(nullable: true),
                    Sw = table.Column<int>(nullable: true),
                    Ogd = table.Column<int>(nullable: true),
                    A = table.Column<int>(nullable: true),
                    Zyw = table.Column<int>(nullable: true),
                    S = table.Column<int>(nullable: true),
                    Wt = table.Column<int>(nullable: true),
                    Sz = table.Column<int>(nullable: true),
                    Mag = table.Column<int>(nullable: true),
                    Po = table.Column<int>(nullable: true),
                    Pp = table.Column<int>(nullable: true),
                    AWw = table.Column<int>(nullable: true),
                    AUs = table.Column<int>(nullable: true),
                    AK = table.Column<int>(nullable: true),
                    AOdp = table.Column<int>(nullable: true),
                    AZr = table.Column<int>(nullable: true),
                    ASw = table.Column<int>(nullable: true),
                    AOgd = table.Column<int>(nullable: true),
                    AA = table.Column<int>(nullable: true),
                    AZyw = table.Column<int>(nullable: true),
                    AS = table.Column<int>(nullable: true),
                    AWt = table.Column<int>(nullable: true),
                    ASz = table.Column<int>(nullable: true),
                    AMag = table.Column<int>(nullable: true),
                    APo = table.Column<int>(nullable: true),
                    APp = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stats", x => x.StatId);
                    table.ForeignKey(
                        name: "FK_Stats_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "CharacterId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Characters_PoprzedniaProfesjaProfessionId",
                table: "Characters",
                column: "PoprzedniaProfesjaProfessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_ProfesjaProfessionId",
                table: "Characters",
                column: "ProfesjaProfessionId");

            migrationBuilder.CreateIndex(
                name: "IX_PromotionProfessions_NextProfessionId",
                table: "PromotionProfessions",
                column: "NextProfessionId");

            migrationBuilder.CreateIndex(
                name: "IX_Stats_CharacterId",
                table: "Stats",
                column: "CharacterId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PromotionProfessions");

            migrationBuilder.DropTable(
                name: "Stats");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Professions");
        }
    }
}
