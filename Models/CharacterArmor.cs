﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class CharacterArmor
    {
        public int CharacterId { get; set; }
        public Character Character { get; set; }
        public int ArmorId { get; set; }
        public Armor Armor { get; set; }
    }
}
