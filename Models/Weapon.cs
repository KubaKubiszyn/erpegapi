﻿using Erpeg.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class Weapon : IStatBonus
    {
        public int WeaponId { get; set; }
        public string Nazwa { get; set; }
        public int Obciazenie { get; set; }
        public string Kategoria { get; set; }
        public string SilaBroni { get; set; }
        public int Zasieg { get; set; }
        public int Przeladowanie { get; set; }
        public string CechyOreza { get; set; }
        public ICollection<CharacterWeapon> CharacterWeapons { get; set; }
        public int? StatBonusId { get; set; }
        public StatBonus StatBonus { get; set; }
    }
}
