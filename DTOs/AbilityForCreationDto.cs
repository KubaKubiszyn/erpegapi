﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class AbilityForCreationDto
    {
        public string Name { get; set; }
        public string Opis { get; set; }
    }
}
