﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class PromotionProfessionDetailsDto
    {
        public int NextProfessionId { get; set; }

        public Profession NextProfession { get; set; }
    }
}
