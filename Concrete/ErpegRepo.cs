﻿using Erpeg.Abstract;
using Erpeg.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Erpeg.Concrete
{
    public class ErpegRepo : IErpegRepo
    {
        private readonly ErpegContext _context;

        public ErpegRepo(ErpegContext context)
        {
            _context = context;
        }

        public async Task<T> GetByIdAsync<T>(int id) where T : class
        {
           return await _context.Set<T>().FindAsync(id);

        }


        public T GetById<T>(int id) where T : class
        {
            return _context.Set<T>().Find(id);
        }

        public async Task<IEnumerable<T>> GetAllAsync<T> () where T : class
        {
            return await _context.Set<T>().ToListAsync();
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return  _context.Set<T>();
        }

        public ICollection<T> FindAll<T>(Expression<Func<T, bool>> match) where T : class
        {
            return _context.Set<T>().Where(match).ToList();
        }

        public async Task<ICollection<T>> FindAllAsync<T>(Expression<Func<T, bool>> match) where T : class
        {
            return await _context.Set<T>().Where(match).ToListAsync();
        }

        public void Add<T>(T entity) where T : class
        {
            _context.AddAsync(entity);
        }

        public IQueryable<T> GetAllIncluding<T>(params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            IQueryable<T> queryable = GetAll<T>();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return queryable;

        }

        public async Task<List<T>> GetAllIncludingAsync<T>(params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            IQueryable<T> queryable = GetAll<T>();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return await queryable.ToListAsync();

        }

        public async Task<T> GetIncludingAsync<T>(Expression<Func<T,bool>> condition, params Expression<Func<T, object>>[] includeProperties) where T : class
        {
            IQueryable<T> queryable = _context.Set<T>().Where(condition);

            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include<T, object>(includeProperty);
            }

            return await queryable.SingleOrDefaultAsync();

        }

        public async Task<T> GetWithNestedPropAsync<T>(Expression<Func<T, bool>> condition, Func<IQueryable<T>, IIncludableQueryable<T, object>> include) where T : class
        {
            IQueryable<T> queryable = _context.Set<T>().Where(condition);

            queryable = include(queryable);
        

            return await queryable.SingleOrDefaultAsync();

        }

        public async Task<T> GetAllWithNested<T>(Func<IQueryable<T>, IIncludableQueryable<T, object>> include) where T : class
        {
            IQueryable<T> queryable = _context.Set<T>();

            queryable = include(queryable);


            return await queryable.SingleOrDefaultAsync();

        }


        public T Find<T> (Expression<Func<T, bool>> match) where T : class
        {
            return  _context.Set<T>().SingleOrDefault(match);
        }

        public async Task<T> FindAsync<T>(Expression<Func<T, bool>> match) where T : class
        {
            return await _context.Set<T>().SingleOrDefaultAsync(match);
        }
 
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> Exists<T>(int id) where T : class
        {
             return await _context.Set<T>().FindAsync(id) != null;
        }
    }
}
