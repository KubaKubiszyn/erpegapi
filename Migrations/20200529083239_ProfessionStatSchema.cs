﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erpeg.Migrations
{
    public partial class ProfessionStatSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Abilities_StatBonus_StatBonusId",
                table: "Abilities");

            migrationBuilder.DropForeignKey(
                name: "FK_Armors_StatBonus_StatBonusId",
                table: "Armors");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_StatBonus_StatBonusId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Weapons_StatBonus_StatBonusId",
                table: "Weapons");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StatBonus",
                table: "StatBonus");

            migrationBuilder.RenameTable(
                name: "StatBonus",
                newName: "StatBonuses");

            migrationBuilder.AlterColumn<int>(
                name: "StatBonusId",
                table: "Armors",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "StatBonusId",
                table: "Abilities",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StatBonuses",
                table: "StatBonuses",
                column: "StatBonusId");

            migrationBuilder.CreateTable(
                name: "StatSchema",
                columns: table => new
                {
                    StatSchemaId = table.Column<int>(nullable: false),
                    Ww = table.Column<int>(nullable: true),
                    Us = table.Column<int>(nullable: true),
                    K = table.Column<int>(nullable: true),
                    Odp = table.Column<int>(nullable: true),
                    Zr = table.Column<int>(nullable: true),
                    Sw = table.Column<int>(nullable: true),
                    Ogd = table.Column<int>(nullable: true),
                    A = table.Column<int>(nullable: true),
                    Zyw = table.Column<int>(nullable: true),
                    S = table.Column<int>(nullable: true),
                    Wt = table.Column<int>(nullable: true),
                    Sz = table.Column<int>(nullable: true),
                    Mag = table.Column<int>(nullable: true),
                    Po = table.Column<int>(nullable: true),
                    Pp = table.Column<int>(nullable: true),
                    ProfessionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatSchema", x => x.StatSchemaId);
                    table.ForeignKey(
                        name: "FK_StatSchema_Professions_StatSchemaId",
                        column: x => x.StatSchemaId,
                        principalTable: "Professions",
                        principalColumn: "ProfessionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Abilities_StatBonuses_StatBonusId",
                table: "Abilities",
                column: "StatBonusId",
                principalTable: "StatBonuses",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Armors_StatBonuses_StatBonusId",
                table: "Armors",
                column: "StatBonusId",
                principalTable: "StatBonuses",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_StatBonuses_StatBonusId",
                table: "Items",
                column: "StatBonusId",
                principalTable: "StatBonuses",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Weapons_StatBonuses_StatBonusId",
                table: "Weapons",
                column: "StatBonusId",
                principalTable: "StatBonuses",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Abilities_StatBonuses_StatBonusId",
                table: "Abilities");

            migrationBuilder.DropForeignKey(
                name: "FK_Armors_StatBonuses_StatBonusId",
                table: "Armors");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_StatBonuses_StatBonusId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Weapons_StatBonuses_StatBonusId",
                table: "Weapons");

            migrationBuilder.DropTable(
                name: "StatSchema");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StatBonuses",
                table: "StatBonuses");

            migrationBuilder.RenameTable(
                name: "StatBonuses",
                newName: "StatBonus");

            migrationBuilder.AlterColumn<int>(
                name: "StatBonusId",
                table: "Armors",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "StatBonusId",
                table: "Abilities",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_StatBonus",
                table: "StatBonus",
                column: "StatBonusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Abilities_StatBonus_StatBonusId",
                table: "Abilities",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Armors_StatBonus_StatBonusId",
                table: "Armors",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_StatBonus_StatBonusId",
                table: "Items",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Weapons_StatBonus_StatBonusId",
                table: "Weapons",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
