﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class CharacterItem
    {
        public Character Character { get; set; }
        public int CharacterId { get; set; }
        public Item Item { get; set; }
        public int ItemId { get; set; }
        public int Ilosc { get; set; }
    }
}
