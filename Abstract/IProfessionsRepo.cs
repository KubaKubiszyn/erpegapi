﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Abstract
{
    public interface IProfessionsRepo
    {
        Task<Profession> GetProfession(int id);
        Task<IEnumerable<Profession>> GetProfessions();
        Task<Profession> Profession(Profession profession);
    }
}
