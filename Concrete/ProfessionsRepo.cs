﻿using Erpeg.Abstract;
using Erpeg.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Erpeg.Concrete
{
    public class ProfessionsRepo : IProfessionsRepo
    {
        private readonly IErpegRepo _erpeg;

        public ProfessionsRepo(IErpegRepo erpeg)
        {
            _erpeg = erpeg;
        }
        public async Task<Profession> GetProfession(int id)
        {
            var profession = await _erpeg.GetByIdAsync<Profession>(id);

            return profession;
        }

        public async Task<IEnumerable<Profession>> GetProfessions()
        {
            var professions = await _erpeg.GetAllAsync<Profession>();

            return professions;
        }

        public async Task<IEnumerable<Profession>> GetStartingProfessions()
        {
            var startingProfessions = await _erpeg.FindAllAsync<Profession>(p => p.Startowa);

            return startingProfessions;
        }

        public async Task<IEnumerable<Profession>> GetIncluded()
        {
            // var test = _erpeg.GetAllIncluding<Character>(c => c.Statystyki, c => c.Profesja).ToList();

            // return test;
            return null;
        }

        public Task<Profession> Profession(Profession profession)
        {
            throw new NotImplementedException();
        }
    }
}
