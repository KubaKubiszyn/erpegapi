﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class Skill
    {
        public int SkillId { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public ICollection<CharacterSkill> Characters { get; set; }
    }

}
