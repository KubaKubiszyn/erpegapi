﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class PromotionProfession
    {
        public int CurrentProfessionId { get; set; }
        public Profession CurrentProfession { get; set; }

        public int NextProfessionId { get; set; }

        public Profession NextProfession { get; set; }


    }
}
