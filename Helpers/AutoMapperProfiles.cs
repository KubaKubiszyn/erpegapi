﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erpeg.DTOs;
using Erpeg.Models;

namespace Erpeg.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            #region Character Mappings
            CreateMap<Character, CharacterForCreationDto>();
            CreateMap<CharacterForCreationDto, Character>();
            CreateMap<Character, CharacterForDetailsDto>();
            CreateMap<Character, CharacterForListDto>();
            #endregion

            #region Stat Mappings
            CreateMap<Stat, StatsDto>();
            CreateMap<StatsDto, Stat>();
            #endregion

            #region Profession Mappings
            CreateMap<Profession, ProfessionForListDTo>();
            CreateMap<ProfessionForCreationDto, Profession>();
            CreateMap<PromotionProfession, PromotionProfessionDetailsDto>();
            CreateMap<PromotionProfession, PromotionProfessionForList>();
            #endregion

            #region Ability
            CreateMap<AbilityForCreationDto, Ability>();
            CreateMap<Ability, AbilityForListDto>();
            CreateMap<AbilityForCreationDto, Ability>();
            CreateMap<Ability, AbilityForDetailsDto>();
            #endregion

            #region Skills
            CreateMap<SkillForCreationDto, Skill>();
            CreateMap<Skill, SkillForListDto>();
            CreateMap<SkillForListDto, Skill>();
            CreateMap<Skill, SkillForDetailDto>();
            #endregion

            #region Weapons
            CreateMap<WeaponForCreationDto, Weapon>();
            CreateMap<Weapon, WeaponForListDto>();
            CreateMap<WeaponForCreationDto, Weapon>();
            CreateMap<Weapon, WeaponForDetailsDto>();
            #endregion

            #region Items
            CreateMap<ItemForCreationDto, Item>();
            CreateMap<Item, ItemForListDto>();
            CreateMap<ItemForCreationDto, Item>();
            CreateMap<Item, ItemForDetailDto>();
            #endregion

            #region Armor
            CreateMap<ArmorForCreationDto, Armor>();
            CreateMap<Armor, ArmorForListDto>();
            CreateMap<ArmorForCreationDto, Armor>();
            CreateMap<Armor, ArmorForDetailDto>();
            #endregion




        }
    }
}
