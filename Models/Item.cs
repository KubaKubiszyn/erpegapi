﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class Item
    {
        public int ItemId { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public float Wartosc { get; set; }

        public ICollection<CharacterItem> CharacterItems { get; set; }
        public int? StatBonusId { get; set; }
        public StatBonus StatBonus { get; set; }
    }
}
