﻿using Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class CharacterSkill
    {
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
        public int CharacterId { get; set; }
        public Character Character { get; set; }
        public int Bieglosc { get; set; }

    }
}
