﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class NextProfessionDto
    {
        public int NextProfessionId { get; set; }
        public int CurrentProfessionId { get; set; }
    }
}
