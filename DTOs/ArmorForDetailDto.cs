﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class ArmorForDetailDto
    {
        public int ArmorId { get; set; }
        public string Typ { get; set; }
        public int Obciazenie { get; set; }
        public string LokalizacjaCiala { get; set; }
        public int PunktyZbroii { get; set; }
        public StatBonus StatBonus { get; set; }
    }
}
