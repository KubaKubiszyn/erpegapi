﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Abstract
{
    public interface IStatBonus
    {
        public StatBonus StatBonus { get; set; }
    }
}
