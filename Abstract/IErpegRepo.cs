﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Erpeg.Abstract
{
    public interface IErpegRepo
    {
        Task<T> GetByIdAsync<T>(int id) where T : class;
        T GetById<T>(int id) where T : class;
        IQueryable<T> GetAll<T>() where T : class;
        Task<IEnumerable<T>> GetAllAsync<T>() where T : class;
        T Find<T>(Expression<Func<T, bool>> match) where T : class;
        Task<T> FindAsync<T>(Expression<Func<T, bool>> match) where T : class;
        Task<ICollection<T>> FindAllAsync<T>(Expression<Func<T, bool>> match) where T : class;
        ICollection<T> FindAll<T>(Expression<Func<T, bool>> match) where T : class;
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        IQueryable<T> GetAllIncluding<T>(params Expression<Func<T, object>>[] includeProperties) where T : class;
        Task<List<T>> GetAllIncludingAsync<T>(params Expression<Func<T, object>>[] includeProperties) where T : class;
        Task<T> GetIncludingAsync<T>(Expression<Func<T, bool>> condition, params Expression<Func<T, object>>[] includeProperties) where T : class;
        Task<T> GetWithNestedPropAsync<T>(Expression<Func<T, bool>> condition, Func<IQueryable<T>, IIncludableQueryable<T, object>> include) where T : class;
        Task<T> GetAllWithNested<T>(Func<IQueryable<T>, IIncludableQueryable<T, object>> include) where T : class;
        Task<bool> SaveAll();
        Task<bool> Exists<T>(int id) where T: class;
    }
}
