﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class ProfessionForListDTo
    {
        public int ProfessionId { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public bool Startowa { get; set; }
        public virtual List<PromotionProfessionForList> NextProfessions { get; set; }
    }
}
