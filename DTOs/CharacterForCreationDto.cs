﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erpeg.DTOs
{
    public class CharacterForCreationDto
    {
        public string Imie { get; set; }
        public string KolorOczu { get; set; }
        public string KolorWlosow { get; set; }
        public string ZnakGwiezdny { get; set; }
        public string Plec { get; set; }
        public int? Waga { get; set; }
        public int? Wzrost { get; set; }
        public int? Rodzenstwo { get; set; }
        public string ZnakiSzczegolne { get; set; }
        public string MiejsceUrodzenia { get; set; }
    }
}
