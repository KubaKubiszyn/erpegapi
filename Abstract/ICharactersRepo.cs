﻿using Erpeg.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Abstract
{
    public interface ICharactersRepo
    {
        Task<Character> GetCharacter(int id);
        Task<IEnumerable<Character>> GetCharacters();
        Task<Character> CreateCharacter(Character character);
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<bool> AssignProfession(int characterId, int professionId);
        Task<bool> AddNewItem(int characterId, int itemId);
        Task<bool> AddNewWeapon(int characterId, int weaponId);
        Task<bool> AddNewArmor(int characterId, int armorId);
        Task<bool> AddExp(int characterId, int amount);
        Task<bool> UpgradeSkillProficiency(int characterId, int skillId, int amount);
        Task<bool> UnlockNewAbility(int characterId, int abilityId);
        Task<bool> UnlockNewSkill(int characterId, int skillId);
    }
}
