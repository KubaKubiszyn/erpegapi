﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class ProfessionStat
    {
        public int ProfessionId { get; set; }
        public Profession Profession { get; set; }
        public int? Ww { get; set; }
        public int? Us { get; set; }
        public int? K { get; set; }
        public int? Odp { get; set; }
        public int? Zr { get; set; }
        public int? Sw { get; set; }
        public int? Ogd { get; set; }
        public int? A { get; set; }
        public int? Zyw { get; set; }
        public int? S { get; set; }
        public int? Wt { get; set; }
        public int? Sz { get; set; }
        public int? Mag { get; set; }
        public int? Po { get; set; }
        public int? Pp { get; set; }
    }
}
