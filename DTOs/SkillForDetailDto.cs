﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class SkillForDetailDto
    {
        public int SkillId { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
    }
}
