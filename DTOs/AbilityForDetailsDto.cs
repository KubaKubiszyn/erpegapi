﻿using Erpeg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class AbilityForDetailsDto
    {
        public int AbilityId { get; set; }
        public string Name { get; set; }
        public string Opis { get; set; }
        public StatBonus StatBonus { get; set; }
    }
}
