﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class ProfessionForCreationDto
    {
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public bool Startowa { get; set; }
    }
}
