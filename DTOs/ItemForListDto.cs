﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class ItemForListDto
    {
        public int ItemId { get; set; }
        public string Nazwa { get; set; }
        public float Wartosc { get; set; }
    }
}
