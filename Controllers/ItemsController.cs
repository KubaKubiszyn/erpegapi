﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using Erpeg.Abstract;
using Erpeg.DTOs;
using Erpeg.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Erpeg.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IErpegRepo repo;
        private readonly IMapper mapper;

        public ItemsController(IErpegRepo repo, IMapper mapper)
        {
            this.repo = repo;
            this.mapper = mapper;
        }

        #region Weapons
        [HttpGet("Weapons")]
        public async Task<IActionResult> GetWeapons()
        {
            var weaponsFromRepo = await repo.GetAllIncludingAsync<Weapon>(a => a.StatBonus);

            if (weaponsFromRepo == null) return NotFound();

            var mappedWeapons = mapper.Map<List<WeaponForListDto>>(weaponsFromRepo);

            return Ok(mappedWeapons);
        }

        [HttpGet("Weapons/{id}", Name = "GetWeapon")]
        public async Task<IActionResult> GetWeapon(int id)
        {
            var weapon = await repo.GetIncludingAsync<Weapon>(w => w.WeaponId == id, w => w.StatBonus);

            if (weapon == null) return NotFound("Could not found requested ability");

            var mappedWeapon= mapper.Map<WeaponForDetailsDto>(weapon);

            return Ok(mappedWeapon);

        }

        [HttpPost("Weapons/add")]
        public async Task<IActionResult> CreateWeapon(WeaponForCreationDto weaponDto)
        {
            var mappedWeapon = mapper.Map<Weapon>(weaponDto);

            repo.Add<Weapon>(mappedWeapon);

            if (await repo.SaveAll()) return Ok(mappedWeapon);

            return CreatedAtRoute("GetWeapon", new { id = mappedWeapon.WeaponId }, mappedWeapon);
        }

        [HttpPut("Weapons/{id}")]
        public async Task<IActionResult> UpdateWeapon(int id, WeaponForCreationDto updatedWeapon)
        {
            var weaponFromRepo = await repo.FindAsync<Weapon>(a => a.WeaponId == id);

            mapper.Map(updatedWeapon, weaponFromRepo);

            if (await repo.SaveAll())
                return NoContent();

            throw new System.Exception($"Updating weapon {id} failed on save");
        }

        [HttpDelete("Weapons/{id}")]
        public async Task<IActionResult> DeleteWeapon(int id)
        {
            var weapon = await repo.FindAsync<Weapon>(a => a.WeaponId == id);

            if (weapon == null) return NotFound("Could not delete item with specified Id");

            repo.Delete(weapon);

            if (await repo.SaveAll())
                return Ok();

            return BadRequest("Could not delete the weapon");
        }
        #endregion

        #region Armor
        [HttpGet("Armors")]
        public async Task<IActionResult> GetArmors()
        {
            var armorsFromRepo = await repo.GetAllIncludingAsync<Armor>(a => a.StatBonus);

            if (armorsFromRepo == null) return NotFound();

            var mappedArmors = mapper.Map<List<ArmorForListDto>>(armorsFromRepo);

            return Ok(mappedArmors);
        }

        [HttpGet("Armors/{id}", Name = "GetArmor")]
        public async Task<IActionResult> GetArmor(int id)
        {
            var armor = await repo.GetIncludingAsync<Armor>(w => w.ArmorId == id, w => w.StatBonus);

            if (armor == null) return NotFound("Could not found requested ability");

            var mappedArmor = mapper.Map<ArmorForDetailDto>(armor);

            return Ok(mappedArmor);

        }

        [HttpPost("Armors/add")]
        public async Task<IActionResult> CreateArmor(ArmorForCreationDto armorDto)
        {
            var mappedArmor = mapper.Map<Armor>(armorDto);

            repo.Add<Armor>(mappedArmor);

            if (await repo.SaveAll()) return Ok(mappedArmor);

            return CreatedAtRoute("GetWeapon", new { id = mappedArmor.ArmorId }, mappedArmor);
        }

        [HttpPut("Armors/{id}")]
        public async Task<IActionResult> UpdateAbility(int id, ArmorForCreationDto updatedArmor)
        {
            var armorFromRepo = await repo.FindAsync<Armor>(a => a.ArmorId == id);

            mapper.Map(updatedArmor, armorFromRepo);

            if (await repo.SaveAll())
                return NoContent();

            throw new System.Exception($"Updating armor {id} failed on save");
        }

        [HttpDelete("Armors/{id}")]
        public async Task<IActionResult> DeleteArmor(int id)
        {
            var armor = await repo.FindAsync<Armor>(a => a.ArmorId == id);

            if (armor == null) return NotFound("Could not delete item with specified Id");

            repo.Delete(armor);

            if (await repo.SaveAll())
                return Ok();

            return BadRequest("Could not delete the armor");
        }
        #endregion

        #region Item
        [HttpGet("Items")]
        public async Task<IActionResult> GetItems()
        {
            var itemsFromRepo = await repo.GetAllIncludingAsync<Item>(a => a.StatBonus);

            if (itemsFromRepo == null) return NotFound();

            var mappedItems = mapper.Map<List<ItemForListDto>>(itemsFromRepo);

            return Ok(mappedItems);
        }

        [HttpGet("Items/{id}", Name = "GetItem")]
        public async Task<IActionResult> GetItem(int id)
        {
            var item = await repo.GetIncludingAsync<Armor>(w => w.ArmorId == id, w => w.StatBonus);

            if (item == null) return NotFound("Could not found requested item");

            var mappedItems = mapper.Map<ArmorForDetailDto>(item);

            return Ok(mappedItems);

        }

        [HttpPost("Items/add")]
        public async Task<IActionResult> CreateItem(ItemForCreationDto itemDto)
        {
            var mappedItem = mapper.Map<Item>(itemDto);

            repo.Add<Item>(mappedItem);

            if (await repo.SaveAll()) return Ok(mappedItem);

            return CreatedAtRoute("GetItem", new { id = mappedItem.ItemId }, mappedItem);
        }

        [HttpPut("Items/{id}")]
        public async Task<IActionResult> UpdateItem(int id, ItemForCreationDto updatedItem)
        {
            var itemFromRepo = await repo.FindAsync<Item>(a => a.ItemId == id);

            mapper.Map(updatedItem, itemFromRepo);

            if (await repo.SaveAll())
                return NoContent();

            throw new System.Exception($"Updating item {id} failed on save");
        }

        [HttpDelete("Items/{id}")]
        public async Task<IActionResult> DeleteItem(int id)
        {
            var item = await repo.FindAsync<Item>(a => a.ItemId == id);

            if (item == null) return NotFound("Could not delete item with specified Id");

            repo.Delete(item);

            if (await repo.SaveAll())
                return Ok();

            return BadRequest("Could not delete the item");
        }
        #endregion


    }
}