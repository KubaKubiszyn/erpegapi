﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class WeaponForCreationDto
    {
        public string Nazwa { get; set; }
        public int Obciazenie { get; set; }
        public string Kategoria { get; set; }
        public string SilaBroni { get; set; }
        public int Zasieg { get; set; }
        public int Przeladowanie { get; set; }
        public string CechyOreza { get; set; }
    }
}
