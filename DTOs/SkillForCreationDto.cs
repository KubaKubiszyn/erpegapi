﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.DTOs
{
    public class SkillForCreationDto
    {
        public string Nazwa { get; set; }
        public string Opis { get; set; }
    }
}
