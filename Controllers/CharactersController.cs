﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Erpeg.Abstract;
using Erpeg.Concrete;
using Erpeg.DTOs;
using Erpeg.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Erpeg.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly ICharactersRepo charactersRepo;
        private readonly IMapper mapper;

        public CharactersController(ICharactersRepo charactersRepo, IMapper mapper)
        {
            this.charactersRepo = charactersRepo;
            this.mapper = mapper;
        }


        [HttpGet]
        public async Task<IActionResult> GetCharacters()
        {
            var characters = await charactersRepo.GetCharacters();
            var mappedCharacters = mapper.Map<IEnumerable<CharacterForListDto>>(characters);
            return Ok(mappedCharacters);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateCharacter(CharacterForCreationDto character)
        {
            //validation if character exist
            var characterToRegister = mapper.Map<Character>(character);
            var characterToReturn = await charactersRepo.CreateCharacter(characterToRegister);

            return CreatedAtRoute("GetCharacter", new { id = characterToReturn.CharacterId}, characterToReturn);

        }

        [HttpGet("{id}", Name = "GetCharacter")]
        public async Task<IActionResult> GetCharacter(int id)
        {
            var character = await charactersRepo.GetCharacter(id);

            var mappedCharacter = mapper.Map<CharacterForDetailsDto>(character);

            return Ok(mappedCharacter);
        }

        [HttpPost("{characterId}/stats")]
        public async Task<IActionResult> SetStatistics(int characterId, StatsDto stats)
        {
            var character = await charactersRepo.GetCharacter(characterId);

            if (character == null) return NotFound();

            var mappedStats = mapper.Map<Stat>(stats);

            character.Statystyki = mappedStats;

            var statsToReturn = mapper.Map<StatsDto>(mappedStats);

            if (await charactersRepo.SaveAll())
                return Ok(statsToReturn);

            return BadRequest("Could not add the stats");
        }

        [HttpPost("{characterId}/Profession/Change/{professionId}")]
        public async Task<IActionResult> SetProfession(int characterId, int professionId)
        {
            if (await charactersRepo.AssignProfession(characterId, professionId))
                return Ok();

            return BadRequest("Could not set specified profession for this character");
        }


        [HttpPost("{characterId}/Skills/Add/{skillId}")]
        public async Task<IActionResult> UnlockSkill(int characterId, int skillId)
        {
            if (await charactersRepo.UnlockNewSkill(characterId, skillId))
                return Ok();

            return BadRequest("Could not set specified skill for this character");
        }

        [HttpPost("{characterId}/Abilities/Add/{abilityId}")]
        public async Task<IActionResult> UnlockAbility(int characterId, int abilityId)
        {
            if (await charactersRepo.UnlockNewAbility(characterId, abilityId))
                return Ok();

            return BadRequest("Could not set specified ability for this character");
        }

        [HttpPost("{characterId}/Items/Add/{itemId}")]
        public async Task<IActionResult> AddNewItem(int characterId, int itemId)
        {
            if (await charactersRepo.AddNewItem(characterId, itemId))
                return Ok();

            return BadRequest("Could not set specified item for this character");
        }

        [HttpPost("{characterId}/Weapons/Add/{weaponId}")]
        public async Task<IActionResult> AddNewWeapon(int characterId, int weaponId)
        {
            if (await charactersRepo.AddNewWeapon(characterId, weaponId))
                return Ok();

            return BadRequest("Could not set specified weapon for this character");
        }

        [HttpPost("{characterId}/Armors/Add/{armorId}")]
        public async Task<IActionResult> AddNewArmor(int characterId, int armorId)
        {
            if (await charactersRepo.AddNewArmor(characterId, armorId))
                return Ok();

            return BadRequest("Could not set specified armor for this character");
        }

    }
}