﻿using Erpeg.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class Armor : IStatBonus
    {
        public int ArmorId { get; set; }
        public string Typ { get; set; }
        public int Obciazenie { get; set; }
        public string LokalizacjaCiala { get; set; }
        public int PunktyZbroii { get; set; }
        public ICollection<CharacterArmor> CharacterArmors { get; set; }
        public int? StatBonusId { get; set; }
        public StatBonus StatBonus { get; set; }
    }
}
