﻿using Erpeg.Migrations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erpeg.Models
{
    public class Profession
    {
        public int ProfessionId { get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public bool Startowa { get; set; }
        public StatSchema StatSchema { get; set; }
        public virtual List<PromotionProfession> NextProfessions { get; set; }
    }
}
