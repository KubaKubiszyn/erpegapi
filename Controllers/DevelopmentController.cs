﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Erpeg.Abstract;
using Erpeg.Concrete;
using Erpeg.DTOs;
using Erpeg.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Erpeg.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevelopmentController : ControllerBase
    {
        private readonly IErpegRepo repo;
        private readonly IMapper mapper;

        public DevelopmentController(IErpegRepo repo, IMapper mapper)
        {
            this.repo = repo;
            this.mapper = mapper;
        }

        #region Abilities
        [HttpGet("abilities")]
        public async Task<IActionResult> GetAbilities()
        {
            var abilitiesFromRepo = await repo.GetAllIncludingAsync<Ability>(a => a.StatBonus);

            if (abilitiesFromRepo == null) return NotFound();

            var mappedAbilities = mapper.Map<List<AbilityForListDto>>(abilitiesFromRepo);

            return Ok(mappedAbilities);
        }

        [HttpGet("abilities/{id}", Name = "GetAbility")]
        public async Task<IActionResult> GetAbility(int id)
        {
            var ability = await repo.GetIncludingAsync<Ability>(a => a.AbilityId == id, a => a.StatBonus);

            if (ability == null) return NotFound("Could not found requested ability");

            var mappedAbility = mapper.Map<AbilityForDetailsDto>(ability);

            return Ok(mappedAbility);

        }

        [HttpPost("abilities/add")]
        public async Task<IActionResult> CreateAbility(AbilityForCreationDto abilityDto)
        {
            var mappedAbility = mapper.Map<Ability>(abilityDto);

            repo.Add<Ability>(mappedAbility);

            if (await repo.SaveAll()) return Ok(mappedAbility);

            return CreatedAtRoute("GetAbility", new { id = mappedAbility.AbilityId }, mappedAbility);
        }

        [HttpPut("abilities/{id}")]
        public async Task<IActionResult> UpdateAbility(int id, AbilityForCreationDto updatedAbility)
        {
            var abilityFromRepo = await repo.FindAsync<Ability>(a => a.AbilityId == id);

            mapper.Map(updatedAbility, abilityFromRepo);

            if (await repo.SaveAll())
                return NoContent();

            throw new System.Exception($"Updating ability {id} failed on save");
        }

        [HttpDelete("abilities/{id}")]
        public async Task<IActionResult> DeleteAbility(int id)
        {
            var ability = await repo.FindAsync<Ability>(a => a.AbilityId == id);

            if (ability == null) return NotFound("Could not delete ability with specified Id");

            repo.Delete(ability);

            if (await repo.SaveAll())
                return Ok();

            return BadRequest("Could not delete the photo");
        } 
        #endregion

        [HttpGet("skills")]
        public async Task<IActionResult> GetSkills()
        {
            var skillsFromRepo = await repo.GetAllAsync<Skill>();

            if (skillsFromRepo == null) return NotFound();

            var mappedSkills = mapper.Map<List<SkillForListDto>>(skillsFromRepo);

            return Ok(mappedSkills);
        }

        [HttpGet("skills/{id}", Name = "GetSkill")]
        public async Task<IActionResult> GetSkill(int id)
        {
            var skill = await repo.GetIncludingAsync<Skill>(s => s.SkillId == id);

            if (skill == null) return NotFound("Could not found requested skill");

            var mappedSkill = mapper.Map<Skill>(skill);

            return Ok(mappedSkill);

        }

        [HttpPost("skills/add")]
        public async Task<IActionResult> CreateSkill(SkillForCreationDto skillDto)
        {
            var mappedSkill = mapper.Map<Skill>(skillDto);

            repo.Add<Skill>(mappedSkill);

            if (await repo.SaveAll()) return Ok(mappedSkill);

            return CreatedAtRoute("GetSkill", new { id = mappedSkill.SkillId}, mappedSkill);
        }

        [HttpPut("skills/{id}")]
        public async Task<IActionResult> UpdateSkill(int id, SkillForCreationDto updatedSkill)
        {
            var skillFromRepo = await repo.FindAsync<Ability>(a => a.AbilityId == id);

            mapper.Map(updatedSkill, skillFromRepo);

            if (await repo.SaveAll())
                return NoContent();

            throw new System.Exception($"Updating user {id} failed on save");
        }

        [HttpDelete("skills/{id}")]
        public async Task<IActionResult> DeleteSkill(int id)
        {
            var skill = await repo.FindAsync<Skill>(s => s.SkillId == id);

            if (skill == null) return NotFound("Could not find skill with specified Id");

            repo.Delete(skill);

            if (await repo.SaveAll())
                return Ok();

            return BadRequest("Could not delete the skill");
        }


    }
}