﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Erpeg.Abstract;
using Erpeg.DTOs;
using Erpeg.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Erpeg.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessionsController : ControllerBase
    {
        private readonly IErpegRepo repo;
        private readonly IMapper mapper;

        public ProfessionsController(IErpegRepo repo, IMapper mapper)
        {
            this.repo = repo;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetProfessions()
        {
            var professionsFromRepo = await repo.GetAllIncludingAsync<Profession>(p => p.NextProfessions);

            var mappedProfessions = mapper.Map<IEnumerable<ProfessionForListDTo>>(professionsFromRepo);

            return Ok(mappedProfessions);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProfession(int id)
        {
            var profession = await repo.GetIncludingAsync<Profession>(condition: p => p.ProfessionId == id,  p=> p.NextProfessions);

            if (profession == null) return NotFound();

            var professionToReturn = mapper.Map<ProfessionForListDTo>(profession);

            return Ok(professionToReturn);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateProfession(ProfessionForCreationDto profession)
        {
            var mappedProfession = mapper.Map<Profession>(profession);

            repo.Add<Profession>(mappedProfession);

            var createdProfession = mapper.Map<ProfessionForListDTo>(mappedProfession);

            if (! await repo.SaveAll())
                return BadRequest("Could not create a profession");

            return Ok(mappedProfession);
            
        }

        [HttpPost("{id}/promotions/{promotionId}")]
        public async Task<IActionResult> CreateNextProfession(int id, int promotionId)
        {
            var profession = new PromotionProfession()
            {
                CurrentProfessionId = id,
                NextProfessionId = promotionId
            };

            repo.Add<PromotionProfession>(profession);

            if (await repo.SaveAll()) return Ok(profession);

            return BadRequest("Could not add new profession");

        }
    }
}