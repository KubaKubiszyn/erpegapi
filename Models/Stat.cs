﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erpeg.Models
{
    public class Stat
    {
        public int StatId { get; set; }
        public Character Character { get; set; }
        public int CharacterId { get; set; }
        public int? Ww { get; set; }
        public int? Us { get; set; }
        public int? K { get; set; }
        public int? Odp { get; set; }
        public int? Zr { get; set; }
        public int? Sw { get; set; }
        public int? Ogd { get; set; }
        public int? A { get; set; }
        public int? Zyw { get; set; }
        public int? S { get; set; }
        public int? Wt { get; set; }
        public int? Sz { get; set; }
        public int? Mag { get; set; }
        public int? Po { get; set; }
        public int? Pp { get; set; }
        public int? AWw { get; set; }
        public int? AUs { get; set; }
        public int? AK { get; set; }
        public int? AOdp { get; set; }
        public int? AZr { get; set; }
        public int? ASw { get; set; }
        public int? AOgd { get; set; }
        public int? AA { get; set; }
        public int? AZyw { get; set; }
        public int? AS { get; set; }
        public int? AWt { get; set; }
        public int? ASz { get; set; }
        public int? AMag { get; set; }
        public int? APo { get; set; }
        public int? APp { get; set; }
    }
}
