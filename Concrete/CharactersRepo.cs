﻿using Erpeg.Abstract;
using Erpeg.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace Erpeg.Concrete
{
    public class CharactersRepo : ICharactersRepo
    {
        private readonly ErpegContext _context;
        private readonly IErpegRepo repo;

        public CharactersRepo(ErpegContext context, IErpegRepo repo)
        {
            _context = context;
            this.repo = repo;
        }

        public async Task<Character> CreateCharacter(Character character)
        {
            await _context.AddAsync(character);
            await _context.SaveChangesAsync();

            return character;
        }

        public async Task<Character> GetCharacter(int id)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == id, 
                    c => c.Skills,
                    c => c.Abilities,
                    c => c.Profesja,
                    c => c.PoprzedniaProfesja,
                    c => c.Items,
                    c => c.Weapons,
                    c => c.Armors);

            return character;
        }

        public async Task<IEnumerable<Character>> GetCharacters()
        {
            var users = await _context.Characters.Include(c => c.Profesja).ToListAsync();

            return users;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> AssignProfession(int characterId, int professionId)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c => c.Profesja, c => c.PoprzedniaProfesja);

            if (character == null) return false;

            var profession = await repo.FindAsync<Profession>(p => p.ProfessionId == professionId);

            if (profession == null) return false;

            if (profession == character.Profesja) return false; 

            if (character.Profesja != null)
                character.PoprzedniaProfesja = character.Profesja;
            character.Profesja = profession;

            return (await repo.SaveAll());
        }

        public async Task<bool> UnlockNewSkill (int characterId, int skillId)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c=> c.Skills);

            if (character == null) return false;

            if (!await repo.Exists<Skill>(skillId)) return false;

            character.Skills.Add(
                    new CharacterSkill { CharacterId = character.CharacterId, SkillId = skillId, Bieglosc = 0 });

            return (await repo.SaveAll());
        }


        public async Task<bool> UnlockNewAbility(int characterId, int abilityId)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c => c.Abilities);

            if (character == null) return false;

            if (!await repo.Exists<Ability>(abilityId)) return false;

            character.Abilities.Add(
                    new CharacterAbility { CharacterId = character.CharacterId, AbilityId = abilityId});

            return (await repo.SaveAll());
        }

        public async Task<bool> UpgradeSkillProficiency(int characterId, int skillId, int amount)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c => c.Skills);

            if (character == null) return false;

            var skill = character.Skills.FirstOrDefault(s => s.SkillId == skillId).Bieglosc += amount;

            return (await repo.SaveAll());
        }

        #region Items, Weapons, Armors
        public async Task<bool> AddNewItem(int characterId, int itemId)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c => c.Items);

            if (character == null) return false;

            if (!await repo.Exists<Item>(itemId)) return false;

            character.Items.Add(
                    new CharacterItem { CharacterId = character.CharacterId, ItemId = itemId });

            return (await repo.SaveAll());
        }

        public async Task<bool> AddNewWeapon(int characterId, int weaponId)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c => c.Weapons);

            if (character == null) return false;

            if (!await repo.Exists<Weapon>(weaponId)) return false;

            character.Weapons.Add(
                    new CharacterWeapon { CharacterId = character.CharacterId, WeaponId = weaponId });

            return (await repo.SaveAll());
        }

        public async Task<bool> AddNewArmor(int characterId, int armorId)
        {
            var character = await repo.GetIncludingAsync<Character>(c => c.CharacterId == characterId, c => c.Armors);

            if (character == null) return false;

            if (!await repo.Exists<Armor>(armorId)) return false;

            character.Armors.Add(
                    new CharacterArmor { CharacterId = character.CharacterId, ArmorId = armorId });

            return (await repo.SaveAll());
        } 
        #endregion

        public async Task<bool> AddExp(int characterId, int amount)
        {
            var character = await repo.FindAsync<Character>(c => c.CharacterId == characterId);

            if (character == null) return false;

            character.DoswiadczenieObecne += amount;
            character.DoswiadczenieRazem += amount;

            return (await repo.SaveAll());
        }

        private void AddBonuses (Character character, IStatBonus bonus)
        {
            character.Statystyki.Ww += bonus.StatBonus.Ww.GetValueOrDefault();
            character.Statystyki.Us += bonus.StatBonus.Us.GetValueOrDefault();
            character.Statystyki.K += bonus.StatBonus.K.GetValueOrDefault();
            character.Statystyki.Odp += bonus.StatBonus.Odp.GetValueOrDefault();
            character.Statystyki.Zr += bonus.StatBonus.Zr.GetValueOrDefault();
            character.Statystyki.Sw += bonus.StatBonus.Sw.GetValueOrDefault();
            character.Statystyki.Ogd += bonus.StatBonus.Ogd.GetValueOrDefault();
            character.Statystyki.A += bonus.StatBonus.A.GetValueOrDefault();
            character.Statystyki.Zyw += bonus.StatBonus.Zyw.GetValueOrDefault();
            character.Statystyki.S += bonus.StatBonus.Wt.GetValueOrDefault();
            character.Statystyki.Sz += bonus.StatBonus.Sz.GetValueOrDefault();
            character.Statystyki.Mag += bonus.StatBonus.Mag.GetValueOrDefault();
            character.Statystyki.Po += bonus.StatBonus.Po.GetValueOrDefault();
            character.Statystyki.Pp += bonus.StatBonus.Pp.GetValueOrDefault();

            character.Statystyki.AWw += bonus.StatBonus.Ww.GetValueOrDefault();
            character.Statystyki.AUs += bonus.StatBonus.Us.GetValueOrDefault();
            character.Statystyki.AK += bonus.StatBonus.K.GetValueOrDefault();
            character.Statystyki.AOdp += bonus.StatBonus.Odp.GetValueOrDefault();
            character.Statystyki.AZr += bonus.StatBonus.Zr.GetValueOrDefault();
            character.Statystyki.ASw += bonus.StatBonus.Sw.GetValueOrDefault();
            character.Statystyki.AOgd += bonus.StatBonus.Ogd.GetValueOrDefault();
            character.Statystyki.AA += bonus.StatBonus.A.GetValueOrDefault();
            character.Statystyki.AZyw += bonus.StatBonus.Zyw.GetValueOrDefault();
            character.Statystyki.AS += bonus.StatBonus.Wt.GetValueOrDefault();
            character.Statystyki.ASz += bonus.StatBonus.Sz.GetValueOrDefault();
            character.Statystyki.AMag += bonus.StatBonus.Mag.GetValueOrDefault();
            character.Statystyki.APo += bonus.StatBonus.Po.GetValueOrDefault();
            character.Statystyki.APp += bonus.StatBonus.Pp.GetValueOrDefault();
        }
    }
}
