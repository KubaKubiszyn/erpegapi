﻿using Erpeg.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class Ability : IStatBonus
    {
        public int AbilityId { get; set; }
        public string Name { get; set; }
        public string Opis { get; set; }
        public ICollection<CharacterAbility> Characters { get; set; }
        public int? StatBonusId { get; set; }
        public StatBonus StatBonus { get; set; }

    }
}
