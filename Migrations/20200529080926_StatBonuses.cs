﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erpeg.Migrations
{
    public partial class StatBonuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatBonusId",
                table: "Weapons",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatBonusId",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatBonusId",
                table: "Armors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StatBonusId",
                table: "Abilities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "StatBonus",
                columns: table => new
                {
                    StatBonusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ww = table.Column<int>(nullable: true),
                    Us = table.Column<int>(nullable: true),
                    K = table.Column<int>(nullable: true),
                    Odp = table.Column<int>(nullable: true),
                    Zr = table.Column<int>(nullable: true),
                    Sw = table.Column<int>(nullable: true),
                    Ogd = table.Column<int>(nullable: true),
                    A = table.Column<int>(nullable: true),
                    Zyw = table.Column<int>(nullable: true),
                    S = table.Column<int>(nullable: true),
                    Wt = table.Column<int>(nullable: true),
                    Sz = table.Column<int>(nullable: true),
                    Mag = table.Column<int>(nullable: true),
                    Po = table.Column<int>(nullable: true),
                    Pp = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatBonus", x => x.StatBonusId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Weapons_StatBonusId",
                table: "Weapons",
                column: "StatBonusId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_StatBonusId",
                table: "Items",
                column: "StatBonusId");

            migrationBuilder.CreateIndex(
                name: "IX_Armors_StatBonusId",
                table: "Armors",
                column: "StatBonusId");

            migrationBuilder.CreateIndex(
                name: "IX_Abilities_StatBonusId",
                table: "Abilities",
                column: "StatBonusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Abilities_StatBonus_StatBonusId",
                table: "Abilities",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Armors_StatBonus_StatBonusId",
                table: "Armors",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_StatBonus_StatBonusId",
                table: "Items",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Weapons_StatBonus_StatBonusId",
                table: "Weapons",
                column: "StatBonusId",
                principalTable: "StatBonus",
                principalColumn: "StatBonusId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Abilities_StatBonus_StatBonusId",
                table: "Abilities");

            migrationBuilder.DropForeignKey(
                name: "FK_Armors_StatBonus_StatBonusId",
                table: "Armors");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_StatBonus_StatBonusId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Weapons_StatBonus_StatBonusId",
                table: "Weapons");

            migrationBuilder.DropTable(
                name: "StatBonus");

            migrationBuilder.DropIndex(
                name: "IX_Weapons_StatBonusId",
                table: "Weapons");

            migrationBuilder.DropIndex(
                name: "IX_Items_StatBonusId",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Armors_StatBonusId",
                table: "Armors");

            migrationBuilder.DropIndex(
                name: "IX_Abilities_StatBonusId",
                table: "Abilities");

            migrationBuilder.DropColumn(
                name: "StatBonusId",
                table: "Weapons");

            migrationBuilder.DropColumn(
                name: "StatBonusId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "StatBonusId",
                table: "Armors");

            migrationBuilder.DropColumn(
                name: "StatBonusId",
                table: "Abilities");
        }
    }
}
