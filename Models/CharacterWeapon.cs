﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erpeg.Models
{
    public class CharacterWeapon
    {
        public int CharacterId { get; set; }
        public Character Character { get; set; }
        public int WeaponId { get; set; }
        public Weapon Weapon { get; set; }
    }
}
