﻿using Erpeg.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erpeg.Models
{
    public class ErpegContext : DbContext
    {
        public ErpegContext(DbContextOptions<ErpegContext> options) : base(options) { }

        #region DbSets
        public DbSet<Character> Characters { get; set; }
        public DbSet<Stat> Stats { get; set; }
        public DbSet<Profession> Professions { get; set; }
        public DbSet<PromotionProfession> PromotionProfessions { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Ability> Abilities { get; set; }
        public DbSet<Armor> Armors { get; set; }
        public DbSet<Weapon> Weapons { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<StatBonus> StatBonuses { get; set; }
        public DbSet<StatSchema> StatSchemas { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Profession
            modelBuilder.Entity<Profession>()
                .HasOne(p => p.StatSchema)
                .WithOne(s => s.Profession)
                .HasPrincipalKey<Profession>(p => p.ProfessionId)
                .HasForeignKey<StatSchema>(s => s.StatSchemaId); 
            #endregion

            #region StatBonus
            modelBuilder.Entity<StatBonus>()
                .HasKey(s => s.StatBonusId);
            #endregion

            #region CharacterItems
            modelBuilder.Entity<CharacterItem>()
        .HasKey(ci => new { ci.CharacterId, ci.ItemId });

            modelBuilder.Entity<CharacterItem>()
                .HasOne(ci => ci.Character)
                .WithMany(c => c.Items)
                .HasForeignKey(ci => ci.CharacterId);

            modelBuilder.Entity<CharacterItem>()
                .HasOne(ci => ci.Item)
                .WithMany(c => c.CharacterItems)
                .HasForeignKey(c => c.ItemId); 
            #endregion

            #region CharacterArmor
            modelBuilder.Entity<CharacterArmor>()
                .HasKey(cs => new { cs.CharacterId, cs.ArmorId });

            modelBuilder.Entity<CharacterArmor>()
                .HasOne(cs => cs.Character)
                .WithMany(c => c.Armors)
                .HasForeignKey(cs => cs.CharacterId);

            modelBuilder.Entity<CharacterArmor>()
                .HasOne(cs => cs.Armor)
                .WithMany(s => s.CharacterArmors)
                .HasForeignKey(cs => cs.CharacterId); 
            #endregion

            #region CharacterWeapon
            modelBuilder.Entity<CharacterWeapon>()
                .HasKey(cs => new { cs.CharacterId, cs.WeaponId });

            modelBuilder.Entity<CharacterWeapon>()
                .HasOne(cs => cs.Character)
                .WithMany(c => c.Weapons)
                .HasForeignKey(cs => cs.CharacterId);

            modelBuilder.Entity<CharacterWeapon>()
                .HasOne(cs => cs.Weapon)
                .WithMany(s => s.CharacterWeapons)
                .HasForeignKey(cs => cs.CharacterId);
            #endregion

            #region CharacterAbility
            modelBuilder.Entity<CharacterAbility>()
                    .HasKey(cs => new { cs.CharacterId, cs.AbilityId });

            modelBuilder.Entity<CharacterAbility>()
                .HasOne(cs => cs.Character)
                .WithMany(c => c.Abilities)
                .HasForeignKey(cs => cs.CharacterId);

            modelBuilder.Entity<CharacterAbility>()
                .HasOne(cs => cs.Ability)
                .WithMany(s => s.Characters)
                .HasForeignKey(cs => cs.AbilityId);
            #endregion

            #region ChracterSkill
            modelBuilder.Entity<CharacterSkill>()
        .HasKey(cs => new { cs.CharacterId, cs.SkillId });

            modelBuilder.Entity<CharacterSkill>()
                .HasOne(cs => cs.Character)
                .WithMany(c => c.Skills)
                .HasForeignKey(cs => cs.CharacterId);

            modelBuilder.Entity<CharacterSkill>()
                .HasOne(cs => cs.Skill)
                .WithMany(s => s.Characters)
                .HasForeignKey(cs => cs.SkillId);
            #endregion

            #region Character
            modelBuilder.Entity<Character>()
                .HasOne(c => c.Statystyki)
                .WithOne(s => s.Character)
                .HasPrincipalKey<Character>(c => c.CharacterId)
                .HasForeignKey<Stat>(s => s.CharacterId);

            modelBuilder.Entity<Character>()
                .Property(c => c.DoswiadczenieObecne)
                .HasDefaultValue(0);

            modelBuilder.Entity<Character>()
                .Property(c => c.DoswiadczenieRazem)
                .HasDefaultValue(0);
            #endregion

            #region PromotionProfessions
            modelBuilder.Entity<PromotionProfession>()
                    .HasOne(cp => cp.NextProfession)
                    .WithMany()
                    .HasForeignKey(cp => cp.NextProfessionId)
                    .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<PromotionProfession>()
                .HasOne(pp => pp.CurrentProfession)
                .WithMany(t => t.NextProfessions)
                .HasForeignKey(pp => pp.CurrentProfessionId);

            modelBuilder.Entity<PromotionProfession>()
                .HasKey(t => new { t.CurrentProfessionId, t.NextProfessionId }); 
            #endregion
        }
    }
}
