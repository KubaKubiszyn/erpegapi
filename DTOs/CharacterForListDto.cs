﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erpeg.DTOs
{
    public class CharacterForListDto
    {
        public int Id { get; set; }
        public string Imie { get; set; }
        public int? DoswiadczenieObecne { get; set; }
        public int? DoswiadczenieRazem { get; set; }
    }
}
