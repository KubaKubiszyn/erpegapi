﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Erpeg.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        public string Imie { get; set; }
        public string KolorOczu { get; set; }
        public string KolorWlosow { get; set; }
        public string ZnakGwiezdny { get; set; }
        public string Plec { get; set; }
        public int? Waga { get; set; }
        public int? Wzrost { get; set; }
        public int? Rodzenstwo { get; set; }
        public string ZnakiSzczegolne { get; set; }
        public string MiejsceUrodzenia { get; set; }
        public int? DoswiadczenieObecne { get; set; }
        public int? DoswiadczenieRazem { get; set; }
        public Profession Profesja { get; set; }
        public Profession PoprzedniaProfesja { get; set; }
        public Stat Statystyki { get; set; }
        public ICollection<CharacterSkill> Skills { get; set; }
        public ICollection<CharacterAbility> Abilities { get; set; }
        public ICollection<CharacterWeapon> Weapons { get; set; }
        public ICollection<CharacterItem> Items { get; set; }
        public ICollection<CharacterArmor> Armors { get; set; }

    }
}
